package com.argentina.redditbot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.yetanotherx.reddit.RedditPlugin;
import com.yetanotherx.reddit.api.data.CommentData;
import com.yetanotherx.reddit.api.data.LinkData;
import com.yetanotherx.reddit.api.modules.RedditLink;

public class UtilsBot {
	public static String calculateElapsedTime(Date linkDate, Date fileDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z"); // the format of your date
		sdf.setTimeZone(TimeZone.getDefault());
		
		long diff = linkDate.getTime() - fileDate.getTime();
	    long diffSeconds = diff / 1000 % 60;
	    long diffMinutes = diff / (60 * 1000) % 60;
	    long diffHours = diff / (60 * 60 * 1000);
	    int diffInDays = (int) ((linkDate.getTime() - fileDate.getTime()) / (1000 * 60 * 60 * 24));

	    return new String(diffInDays + " dias " + diffHours + " Horas y " + diffMinutes + " Minutos");
	}
	
	public static boolean madeComment(RedditPlugin plug , LinkData link) {
		RedditLink newLin = RedditLink.newFromLink(plug, link);
		for (CommentData dat : newLin.getComments()) {
			if (dat.getAuthor().equals("bitcoin_incident_bot")) {
				return true;
				//throw new DoQuitError("Ya postee aqui, nada que hacer.");
			}
		}
		return false;
	}
	
	public static void deleteIncidentFile() {
		File incidentFile = new File("lastIncident.txt");
		 
		if(incidentFile.delete()){
			System.out.println(incidentFile.getName() + " fue eliminado");
		}else{
			System.out.println("Error al eliminar archivo de incidente.");
		}
		
	}
	
	public static void saveIncidentFile(LinkData link) {

		File incidentFile = new File("lastIncident.txt");

		IncidentData incidentData = new IncidentData();
		incidentData.author = link.getAuthor();
		incidentData.title = link.getTitle();
		incidentData.timestamp = link.getCreated();
		incidentData.link = link.getURL();
		
		FileOutputStream fileOut = null;
		ObjectOutputStream out = null;

		try {
			fileOut = new FileOutputStream(incidentFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			out = new ObjectOutputStream(fileOut);
			out.writeObject(incidentData);
			out.close();
			fileOut.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	
	public static IncidentData readIncidentFile() {
		File incidentFile = new File("lastIncident.txt");
		//FileInputStream dateFile = new FileInputStream(incidentFile);

		if(incidentFile.exists()) {
			FileInputStream fileIn = null;
			ObjectInputStream in = null;
			try {
				fileIn = new FileInputStream(incidentFile);
				in = new ObjectInputStream(fileIn);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			IncidentData incidentData;
			try {
				incidentData = (IncidentData) in.readObject();
				in.close();
				fileIn.close();

				if(incidentData != null) {
					return incidentData;
				}

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


		return null;


	}
	
}
