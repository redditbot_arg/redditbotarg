//******************************************************************************************************
// Creado por /u/GauchoFromLaPampa @Reddit.com
//******************************************************************************************************

package com.argentina.redditbot;

import com.beust.jcommander.JCommander;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.yetanotherx.reddit.RedditPlugin;
import com.yetanotherx.reddit.api.ListType;
import com.yetanotherx.reddit.api.data.CommentData;
import com.yetanotherx.reddit.api.data.LinkData;
import com.yetanotherx.reddit.api.modules.ExternalDomain;
import com.yetanotherx.reddit.api.modules.RedditCore;
import com.yetanotherx.reddit.api.modules.RedditLink;
import com.yetanotherx.reddit.api.modules.RedditSubreddit;
import com.yetanotherx.reddit.redditbot.http.Transport;
import com.yetanotherx.reddit.http.request.Request;
import com.yetanotherx.reddit.http.request.RequestType;
import com.yetanotherx.reddit.http.request.WebRequest;
import com.yetanotherx.reddit.http.response.JSONResult;
import com.yetanotherx.reddit.http.response.Response;
import com.yetanotherx.reddit.util.collections.EasyHashMap;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainPlugin extends RedditPlugin {

	private Parameters params;
	List<Date> dateList = new ArrayList<Date>();
	List<LinkData> linkDataList = new ArrayList<LinkData>();
	Validator validator = new Validator();
	
	public MainPlugin(Parameters params) {
		this.params = params;
	}
	
	private void createLinkLists() {
		dateList.clear();
		linkDataList.clear();
		
		for (LinkData link : ExternalDomain.getArgentinaData(this, ListType.NEW, "argentina")) {

			int level = validator.validateComment(link.getTitle().toLowerCase());
			//System.out.println("link.getTitle() " + link.getTitle() + " level " + level);
			
			if(level > 0) {

				long unixSeconds = link.getCreated();
				Date linkDate = new Date(unixSeconds*1000L);
				//SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z"); // the format of your date
				//sdf.setTimeZone(TimeZone.getDefault());
				
				dateList.add(linkDate);
				linkDataList.add(link);
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
			
		}
		
	}
	
	private void processLinks() {
		Date maxDate = Collections.max(dateList);
		
		 if(maxDate != null) {

			 for(LinkData ld : linkDataList) {
				long unixSeconds = ld.getCreated();
				Date linkDate = new Date(unixSeconds*1000L);
				
				if(linkDate.equals(maxDate)) {
					//System.out.println("maxDate " + maxDate + " titulo: " + ld.getTitle());
					
					File incidentFile = new File("lastIncident.txt");
					//FileInputStream dateFile = new FileInputStream(incidentFile);

					if(!incidentFile.exists()) {
						UtilsBot.saveIncidentFile(ld);
						
						
					} else {
						
						IncidentData incidentData = UtilsBot.readIncidentFile();
						if(incidentData != null) {
							//System.out.println("incidentData " + incidentData.title + " ts: " + incidentData.timestamp);
							
							//long unixSeconds = earlier.getCreated();
							Date fileDate = new Date((long)incidentData.timestamp*1000L);
							//Date fileDate = new Date((long)incidentData.timestamp*1000L);
							//Date tmpDate = new Date();
							
							String tiempo = UtilsBot.calculateElapsedTime(linkDate, fileDate);
							//calculateElapsedTime(tmpDate, fileDate);
							
							if(!ld.getCreated().equals(incidentData.timestamp)) {
								if(!UtilsBot.madeComment(this,ld)) {
									System.out.println("Genera comentario " + ld.getTitle() + " tiempo " + tiempo);

									RedditLink newLin = RedditLink.newFromLink(this, ld);
									//String timeString = "0 dias 5 Horas 53 Minutos";
									String comment = "**Shame on you OP, han transcurrido " + tiempo + " desde el ultimo Bitcoin Incident en** /r/Argentina. Se resetea el contador.  \n\n**Incidente anterior:** " + incidentData.link + "  \n" + "**Autor:** /u/" + incidentData.author + "  \n";
									newLin.doReply(comment); // doReplyToComment(comment)
									
									StringBuffer display = new StringBuffer();
									display.append(comment + "\n  ");
									
									System.out.println(display);
									
									UtilsBot.deleteIncidentFile();
									UtilsBot.saveIncidentFile(ld);
									
									return;
									//delete file
									//save file
									
								}
							}
							
						}
						
						
					}
					
				}
			 }
			 
		 }
		 
	}
	
	private void botAction(Validator bot) {
		
		createLinkLists();
		
		if(linkDataList.size() > 0)
			System.out.println("Numero de links detectados: " + linkDataList.size());
		
		if(dateList.size() > 0) {
			processLinks();
		}
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void run() throws InterruptedException {

		validator.addPattern("bitcoin");
		validator.addPattern("bitcoins");
		validator.addPattern("/r/bitcoin");
		
		//bot.addPattern("8000");

		validator.initPattern();

		boolean login = RedditCore.newFromUserAndPass(this, params.username, params.password).doLogin();
		
		if(login == true) {
			//RedditSubreddit sr = RedditSubreddit.newFromName(this, "argentina");
			
			for(;;) {
				System.out.println("Lurkeando...");
				try {
					botAction(validator);
				} catch (com.yetanotherx.reddit.exception.APIException e) {
					System.out.println("Ha ocurrido un error");
					e.printStackTrace();
					Thread.sleep(40000);
				} catch(com.yetanotherx.reddit.exception.ParserException e) {
					System.out.println("Ha ocurrido un error");
					e.printStackTrace();
					Thread.sleep(40000);
				} catch(Exception e) {
					System.out.println("Ha ocurrido un error");
					e.printStackTrace();
					Thread.sleep(40000);
				}
				
				Thread.sleep(40000);
			}

		}
	}
	
	@Override
	public String getName() {
		return "/r/Argentina Bitcoin Bot";
	}
	
	@Override
	public String getVersion() {
		return "0.1";
	}
	
	public static void main(String[] args) {
		Parameters params = new Parameters();
		JCommander jc = new JCommander(params, args);
		try {
			new MainPlugin(params).run();
		} catch (InterruptedException ex) {
			Logger.getLogger(MainPlugin.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}