package com.argentina.redditbot;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
	private ArrayList<String> patternWordList = new ArrayList<String>();
	Pattern pattern;
	Matcher matcher;
	
	public void addPattern(String word) {
		patternWordList.add(word);		
	}
	
	public void initPattern() {
		String matchPatternString = "\\b(?:";
		for(int i = 0; i < patternWordList.size(); i++) {
			String word = patternWordList.get(i);
			matchPatternString += word;
			if(patternWordList.size()>1 && i < patternWordList.size()-1)
				matchPatternString += "|";
		}
		matchPatternString += ")\\b";
		
		//System.out.println(matchPatternString);
		
		pattern = Pattern.compile(matchPatternString);
		//Matcher matcher = errorPattern.matcher(content);
	}
	
	public int validateComment(String comment) {
		
		int euphoriaCounter = 0;
		
		//System.out.println("Comment: " + comment);
		Matcher matcher = pattern.matcher(comment);
		boolean found = false;
        while (matcher.find()) {
        	//if(!found)
        	//	System.out.println("--------------------------------------------");
        	
            //System.out.format("I found the text" + " \"%s\" starting at " + "index %d and ending at index %d.%n", matcher.group(), matcher.start(), matcher.end());
        //	System.out.format("I found the text" + " \"%s\"  %n", matcher.group());
            found = true;
            euphoriaCounter++;
        }
        
        if(!found){
        	//System.out.format("No match found.%n");
        } else {
        //	System.out.format("euphoriaCounter %d%n", euphoriaCounter);
        //	System.out.println("--------------------------------------------");
        }
        
        
        return euphoriaCounter;
        
	}
	
}
