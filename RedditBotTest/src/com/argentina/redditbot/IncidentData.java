package com.argentina.redditbot;

public class IncidentData implements java.io.Serializable
{
   public String title;
   public String author;
   public String link;
   public int timestamp;
   
}